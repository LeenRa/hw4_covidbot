# ReadMe

The objective of this chatbot ‘محارب كوفيد الآلي’ is to provide essential information to the user about covid-19. It will help spread awareness and defy misconceptions by presenting facts to the user upon request. It is intended for Arabic speaking users only. 

The functionalities of the bot is as follows:

- Give a definition to the virus causing the pandemic.
- List the symptoms of the sickness.
- Inform the user about ways of treating the virus.
- Spread awareness on the ways the virus spreads.
- Inform the user of the range of time the sickness lasts for. 
 
## Environment Setup

Used conda and vertualvenv to setup the environment for this project.

```bash
conda create -n rasa_bot python=3.8
```
Activate environments

```bash
conda activate rasa_bot
source ./rasa/bin/activate 
```

## Saved Model
The model trained on the provided files is saved under the /models directory. 


## Talk to the Bot
```bash
rasa shell 
```


## License
[MIT](https://choosealicense.com/licenses/mit/) 